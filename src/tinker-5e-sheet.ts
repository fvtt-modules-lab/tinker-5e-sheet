import { TinkerActorSheet5eCharacter } from "./ui/Tinker5eActorSheet";

Hooks.on("ready", () => {
  Actors.registerSheet("dnd5e", TinkerActorSheet5eCharacter, {
    types: ["character"],
    makeDefault: false,
  });
});
