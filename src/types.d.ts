declare const ActorSheet5eCharacter: any;

declare module "/systems/dnd5e/module/actor/sheets/character.js";

declare module "/systems/dnd5e/module/actor/sheets/character.js*" {
  export = ActorSheet5eCharacter;
}

//
interface ObjectData {
  name: string;
  img: string;
  data?: Record<string, any> & {
    foo?: string;
    abilities: Record<string, any>;
    skills: Record<string, any>;
  };
  senses?: any;
}
