import Tinker5eActorSheetSvelte from "./Tinker5eActorSheet.svelte";

import ActorSheet5eCharacter from "/systems/dnd5e/module/actor/sheets/character.js";

export class TinkerActorSheet5eCharacter extends ActorSheet5eCharacter {
  _state: number;

  component: Tinker5eActorSheetSvelte;

  public get template(): string {
    return "modules/tinker-5e-sheet/templates/root.html";
  }

  static get defaultOptions(): any {
    return {
      ...super.defaultOptions,
      classes: ["tinker-5e"],
      class: [],
      template: "modules/tinker-5e-sheet/templates/root.html",
      resizable: true,
    };
  }

  submit(): void {
    return;
  }

  render(force: boolean): Application {
    if (this._state <= 0) {
      return super.render(force);
    }
    console.log("render", this.getData());

    this.component.$set({
      ...this.getData(),
    });
  }

  activateListeners(html: JQuery<HTMLElement>): void {
    console.log(this);
    console.log(this.getData());
    this.component = new Tinker5eActorSheetSvelte({
      target: html.get(0),
      props: {
        ...this.getData(),
      },
    });

    this.component.$on("updated", evt => {
      const { name, value } = evt.detail;
      this.actor.update({ [name]: value });
    });
  }
}
