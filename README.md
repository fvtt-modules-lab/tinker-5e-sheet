# Tinker 5e Sheet

The Tinker character sheet is a built-from-scratch character sheet for the dnd5e
Foundry system.

- Grid based
- Stretchy and responsive
- Supports very small window size and mobile view.
- Supports very large window size, but it's not very useful...
- Modern render techniques (Svelte)

## Features in progress

- [ ] Attributes
- [ ] Inventory
- [ ] Features
- [ ] Spellbook
- [ ] Effects
- [ ] Biography
- [ ] Journal
